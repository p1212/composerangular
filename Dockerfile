FROM node:latest as node
WORKDIR /app
COPY . .
RUN npm install
RUN npm run-script build

# stage 2
FROM nginx:alpine
COPY --from=node /app/dist/ /usr/share/nginx/html

# docker system prune -a -f
# docker build -t fabcarAngular .
# docker ps | grep 4200 | awk '{print $1}' | xargs docker stop
# docker run --rm -d -p 4200:80/tcp fabcarAngular

